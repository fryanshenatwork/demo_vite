# vitten
A CLI pack base on vite with vue, vuex, eslint

## Start up
* Download [VisualStudio](https://code.visualstudio.com/)
* Install the following extensions for [VisualStudio](https://code.visualstudio.com/)
    1. [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) 
    1. [Prettier ESLint](https://marketplace.visualstudio.com/items?itemName=rvest.vs-code-prettier-eslint)
    1. [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
* Install needed package via npm
    ```bash
    npm run install
    ```

## Usage
Run dev server
```bash
npm run dev -- --port 8461
```
Build 
```bash
npm run build
```
Tag
```bash
npm run version:update // {Package_Name}_{0.0.0.alpha.*}
npm run version:patch // {Package_Name}_{0.0.*}
npm run version:minor // {Package_Name}_{0.*.0}
npm run version:major // {Package_Name}_{*.0.0}
```
Pack Dist
```bash
npm run pack
```