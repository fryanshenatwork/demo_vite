import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index.ts'
import store from './plugins/store/index.ts'
import i18n from './plugins/i18n'
import '/@/assets/style/index.scss'
import 'sweetalert2/dist/sweetalert2.min.css'
import info from '/@/.info.json'// eslint-disable-line

let app: any = {}
app = createApp(App)
app.use(router)
app.use(i18n)
app.use(store)

router.isReady().then(() => {
  app.mount('#app')
})

app.buildVersion = info.version
window.app = app

const exportApp = app
export default exportApp
