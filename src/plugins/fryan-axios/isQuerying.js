'use strict'
const beforeFetch = function (opt, resolve, reject) {
  if (opt._settings.state) {
    if (opt._fetchState.querying === true) {
      reject(false)
    }
    opt._fetchState.querying = true
  }
  resolve(true)
}

const afterFetch = function (opt, resolve, reject) {
  if (opt._settings.state) {
    opt._fetchState.querying = false
  }
  resolve(true)
}

const errorHappen = function (type) {
  const err = `_fethcing_lifeCycle method not found "${type}"`
  console.log(err)
  // reject(new Error(false))
}

export default function (opt = {
  status: null,
  _settings: null,
  _fetchState: null
}) {
  const status = opt.status

  return new Promise(async function (resolve, reject) {
    if (status === 'beforeFetch') {
      await beforeFetch(opt, resolve, reject)
    } else if (status === 'afterFetch') {
      await afterFetch(opt, resolve, reject)
    } else {
      await errorHappen(opt, resolve, reject)
    }
  })
}
