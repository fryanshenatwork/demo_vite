import single from './fetchSingle'
import all from './fetchAll'

export default { single, all }
