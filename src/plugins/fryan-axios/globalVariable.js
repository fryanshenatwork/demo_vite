'use strict'
import { timeout } from './axiosSettings'

let _fetchState = {
  querying: false,
  success: 0,
  error: 0
}

const _dfSingleOpt = {
  method: 'get',
  url: null,
  data: null, // use for post
  params: null, // use for get
  beforeFetch: null, // lifeCycle beforeFetch
  afterFetch: null, // lifeCycle afterFetch
  beforeFetchCheck: null,
  afterFetchCheck: null,
  state: true, // it will skip all request after this until response
  timeout: timeout,
  notifyWhenError: true,
  refreshWhenError: true,
  _fromAll: false,
  beforeNotify: null
}

const _dfMultipleOpt = {
  list: [],
  beforeFetch: null, // lifeCycle beforeFetch
  afterFetch: null, // lifeCycle afterFetch
  beforeFetchCheck: null,
  afterFetchCheck: null,
  state: true,
  timeout: timeout,
  notifyWhenError: true,
  refreshWhenError: true,
  beforeNotify: null
}

const _defaultAxiosOpt = {
  url: _dfSingleOpt.url,
  method: _dfSingleOpt.method,
  data: _dfSingleOpt.data,
  params: _dfSingleOpt.post,
  timeout: _dfSingleOpt.timeout,
  callback: null
}

export default {}
export {
  _fetchState,
  _dfSingleOpt,
  _dfMultipleOpt,
  _defaultAxiosOpt
}
