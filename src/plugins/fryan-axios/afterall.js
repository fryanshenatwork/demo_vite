'use strict'
import Swal from 'sweetalert2'
import * as utils from './utils'

const isFunction = utils.isFunc

const beforeNotify = async function (opt) {
  const _settings = opt._settings
  const error = opt.error
  let type = error
  if (type.includes('404')) { type = '404' }
  if (type.includes('timeout')) { type = 'timeout' }

  const wait = await new Promise(async (resolve, reject) => {
    if (!isFunction(_settings.beforeNotify)) { resolve(); return false }

    const wait = setTimeout(() => { reject(new Error('timeout')) }, 1000)
    if (await _settings.beforeNotify(type, resolve)) {
      clearTimeout(wait)
    }
  })
    .then((res = true) => { return res })
    .catch(() => { console.error('beforeNotify has no response'); return false })

  return wait
}

const notify = function (opt) {
  const _settings = opt._settings
  const text = opt.text

  if (_settings.notifyWhenError) {
    Swal.fire({
      title: 'Connection Error',
      text: text.join(' ,'),
      icon: 'error',
      confirmButtonText: 'Got it',
      timer: 3000
    })
  }
}

const refresh = function (opt) {
  const _settings = opt._settings
  const text = opt.text

  if (_settings.refreshWhenError) {
    const timer = 10
    Swal.fire({
      title: 'Connection Error',
      html: text + `, page will refresh after <b>${timer - 1}</b> seconds.`,
      timer: timer * 1000,
      timerProgressBar: true,
      icon: 'error',
      confirmButtonText: 'Cancel',
      onBeforeOpen: () => {
        if (window.timer === undefined) { window.timer = {} }
        if (window.timer.fetchPluginTimer === undefined) { window.timer.fetchPluginTimer = '' }

        const timerFun = function (num) {
          Swal.getContent().querySelector('b').textContent = num + 1 > 10 ? 10 : num + 1

          const time = function () {
            window.timer.fetchPluginTimer = setTimeout(() => {
              let _num = Math.round(Number(Swal.getTimerLeft()) / 1000)
              if (_num-- > -1) { timerFun(_num) }
            }, 500)
          }

          if (num > 0) { time() } else {
            window.timer.fetchPluginTimer = setTimeout(() => {
              location.reload()
            }, 1000)
          }
        }

        timerFun(Number(timer))
      },
      onClose: () => {
        clearTimeout(window.timer.fetchPluginTimer)
      }
    })
  }
}

export default async function (obj) {
  const isSuccess = obj.isSuccess
  const _settings = obj._settings
  let error = obj.error

  if (typeof (error) === 'object') { error = obj.error.message }
  if (isSuccess) { return true }

  let text = []
  if (error === 'error') {
    text.push('An error occurred when connecting server')
  } else if (error === 'skip') {
    text.push('This request can not be reach, because last request is still querying')
  } else if (typeof (error) === 'string') {
    text.push(error)
  }

  const check = await beforeNotify({
    isSuccess,
    _settings,
    error,
    text
  })

  if (check) {
    notify({
      isSuccess,
      _settings,
      text
    })

    refresh({
      isSuccess,
      _settings,
      text
    })
  }
}

export {}
