'use strict'
import * as utils from './utils'
const isFunction = utils.isFunc

const beforeFetch = async function (opt, resolve, reject) {
  if (isFunction(opt._settings.beforeFetch)) {
    await opt._settings.beforeFetch()
  }
  resolve(true)
}

const afterFetch = async function (opt, resolve, reject) {
  opt._fetchState[opt.type] += 1
  if (isFunction(opt._settings.afterFetch)) {
    await opt._settings.afterFetch(opt.type)
  }
  resolve(true)
}

const beforeFetchCheck = async function (opt, resolve, reject) {
  if (!isFunction(opt._settings.beforeFetchCheck)) { resolve('Unset'); return }
  const result = await opt._settings.beforeFetchCheck()
  if (result === true) {
    resolve(true)
  } else {
    reject('Process stop in lifeCycle beforeFetchCheck, result is ' + result)
  }
}

const afterFetchCheck = async function (opt, resolve, reject) {
  if (!isFunction(opt._settings.afterFetchCheck)) { resolve('Unset'); return }
  const result = await opt._settings.afterFetchCheck()
  if (result === true) {
    resolve(true)
  } else {
    reject('Process stop in lifeCycle afterFetchCheck, result is ' + result)
  }
}

const errorHappen = function (type) {
  const err = `_fethcing_lifeCycle method not found "${type}"`
  console.log(err)
  // reject(new Error(false))
}

export default function (opt = {
  status: null,
  type: null,
  _settings: null,
  _fetchState: null
}) {
  const status = opt.status

  return new Promise(async function (resolve, reject) {
    if (status === 'beforeFetch') {
      await beforeFetch(opt, resolve, reject)
    } else if (status === 'afterFetch') {
      await afterFetch(opt, resolve, reject)
    } else if (status === 'beforeFetchCheck') {
      await beforeFetchCheck(opt, resolve, reject)
    } else if (status === 'afterFetchCheck') {
      await afterFetchCheck(opt, resolve, reject)
    } else {
      await errorHappen(opt, resolve, reject)
    }
  })
}
