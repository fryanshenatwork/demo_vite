'use strict'
import axios from 'axios'
const timeout = 1000 * 20

export default axios.create({
  timeout: timeout, // timeout 10s
  headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
  // baseURL: ''
  // headers: ''
})
export { timeout }
