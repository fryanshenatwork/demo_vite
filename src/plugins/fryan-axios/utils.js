const isFunc = function (test) {
  return typeof (test) === 'function'
}

const isNumber = (value) => {
  return typeof (value) === 'number' && isFinite(value)
}

export default {}
export {
  isFunc,
  isNumber
}
