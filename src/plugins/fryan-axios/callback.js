'use strict'
import { isFunc } from './utils'

export default function (_axiosOpt, response) {
  if (
    _axiosOpt.callback &&
    isFunc(_axiosOpt.callback)
  ) {
    _axiosOpt.callback(response)
  }
}
