'use strict'
import qs from 'qs'

const dataWrong = (data) => {
  if (['', undefined, null, {}].includes(data)) { return true }
  if (typeof (data) !== 'object') {
    console.warn('params must be an object')
  }
  const check = data === undefined ||
  data === '' ||
  data === null ||
  data === false ||
  !typeof (data) === 'object'
  return check
}

const post = function (data) {
  if (dataWrong(data)) { return '' }
  return qs.stringify(data)
}

const get = function (data) {
  if (dataWrong(data)) { return '' }
  let obj = {}
  Object.keys(data).forEach(e => {
    const key = e
    const value = data[e]
    obj[key] = encodeURIComponent(JSON.stringify(value))
  })
  return obj
}
export default {}
export { get, post }
