'use strict'
import $ from 'jquery'
import lifeCycle from './lifeCycle'
import callback from './callback'
import responseFormatter from './responseFormatter'
import afterAll from './afterall'
import isQuerying from './isQuerying'
import cusAxios from './axiosSettings'
import * as dataFormatter from './dataFomatter'
import { isNumber } from './utils'
import { _fetchState, _dfSingleOpt, _defaultAxiosOpt } from './globalVariable'

export default function (userOpt) {
  return new Promise(async (resolve, reject) => {
    const _settings = Object.assign({}, _dfSingleOpt, userOpt)

    // beforeFetchCheck
    const beforeFetchCheck = await lifeCycle({
      status: 'beforeFetchCheck',
      type: null,
      _settings,
      _fetchState
    })
      .then(res => { return true })
      .catch(ers => { reject(ers); return false })
    if (!beforeFetchCheck) {
      afterAll({
        isSuccess: false,
        error: 'beforeFetchCheck',
        _settings,
        _fetchState
      })
      return false
    }

    // beforeFetch
    await lifeCycle({
      status: 'beforeFetch',
      type: null,
      _settings,
      _fetchState
    })
      .then(res => { return true })
      .catch(ers => { reject(ers); return false })
    //

    // setup _axiosOpts
    const _axiosOpts = $.extend(true, {}, _defaultAxiosOpt, {
      url: _settings.url,
      method: _settings.method,
      data: dataFormatter.post(_settings['data']),
      params: dataFormatter.get(_settings['params']),
      callback: _settings.callback
    })
    if (isNumber(_settings.timeout)) {
      _axiosOpts['timeout'] = _settings.timeout
    } else {
      reject(
        new Error('An Error occurred when setting timeout, value it must be number', _settings.timeout)
      )
      return false
    }
    //

    // fetching
    let _response = ''
    const fetching = async function () {
      // change _state querying
      const isQueryingBefore = await isQuerying({
        status: 'beforeFetch',
        _settings,
        _fetchState
      })
        .then(res => { return true })
        .catch(ers => { return false })
      if (!isQueryingBefore) {
        _response = 'skip'
        return 'skip'
      }
      //

      const fetched = await cusAxios(_axiosOpts)
        .then(async response => {
          _response = response
          return 'success'
        })
        .catch(async ers => {
          _response = ers
          return 'error'
        })

      return fetched
    }
    const queryResult = await fetching()
    //

    // change _state querying
    await isQuerying({
      status: 'afterFetch',
      _settings,
      _fetchState
    })
      .then(res => { return true })
      .catch(ers => { return false })
    //

    // afterFetch
    await lifeCycle({
      status: 'afterFetch',
      type: queryResult,
      _settings,
      _fetchState
    }) // lifeCycle afterFetch

    const isSuccess = queryResult === 'success'

    // afterFetchCheck
    const afterFetchCheck = await lifeCycle({
      status: 'afterFetchCheck',
      type: null,
      _settings,
      _fetchState
    })
      .then(res => { return true })
      .catch(ers => { reject(ers); return false })
    if (!afterFetchCheck) {
      afterAll({
        isSuccess: false,
        error: 'afterFetchCheck',
        _settings,
        _fetchState
      })
      return false
    }
    //

    if (isSuccess) {
      const successFlow = async function () {
        if (afterFetchCheck) {
          await callback(_axiosOpts, _response)
          await resolve(
            responseFormatter(_response)
          )
        }
      }
      successFlow()
    } else {
      const errorFlow = async function () {
        await reject(_response)
      }
      errorFlow()
    }

    if (_settings._fromAll === false) {
      afterAll({
        type: 'single',
        isSuccess,
        error: _response,
        _settings,
        _fetchState
      })
    }

    return isSuccess
  }) // Promise
} // single
