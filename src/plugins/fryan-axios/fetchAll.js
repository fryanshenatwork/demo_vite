'use strict'
import $ from 'jquery'
import lifeCycle from './lifeCycle'
import afterAll from './afterall'
import isQuerying from './isQuerying'
import single from './fetchSingle'
import { _fetchState, _dfMultipleOpt, _defaultAxiosOpt } from './globalVariable'

export default function (userOpt) {
  return new Promise(async (resolve, reject) => {
    // generating _settings
    const _defaulting = $.extend(true, {}, _dfMultipleOpt, userOpt)

    let _defaultingList = { list: [] }
    _defaulting['list'].forEach(function (singleRequest) {
      _defaultingList['list'].push(
        $.extend(true, {}, _defaultAxiosOpt, singleRequest, {
          beforeFetch: null,
          afterFetch: null,
          beforeFetchCheck: _defaulting.beforeFetchCheck,
          afterFetchCheck: _defaulting.afterFetchCheck,
          state: false,
          notifyWhenError: false,
          refreshWhenError: false,
          _fromAll: true
        })
      )
    })
    const _settings = $.extend(true, {}, _defaulting, _defaultingList)
    //

    // beforeFetchCheck
    const beforeFetchCheck = await lifeCycle({
      status: 'beforeFetchCheck',
      type: null,
      _settings,
      _fetchState
    })
      .then(res => { return true })
      .catch(ers => { reject(ers); return false })
    if (!beforeFetchCheck) {
      afterAll({
        isSuccess: false,
        error: 'beforeFetchCheck',
        _settings,
        _fetchState
      })
      return false
    }

    // lifeCycle beforeFetch
    await lifeCycle({
      status: 'beforeFetch',
      type: null,
      _settings,
      _fetchState
    })
      .then(res => { return true })
      .catch(ers => { reject(ers); return false })

    // fetching
    let _response = ''
    const fetching = async function () {
      // change _state querying
      const isQueryingBefore = await isQuerying({
        status: 'beforeFetch',
        _settings,
        _fetchState
      })
        .then(res => { return true })
        .catch(ers => { return false })
      if (!isQueryingBefore) {
        _response = 'skip'
        return 'skip'
      }
      //

      const fetched = await Promise.all(_settings.list.map(sreq => {
        return single(sreq)
      }))
        .then(async responses => {
          _response = responses
          return 'success'
        })
        .catch(async ers => {
          _response = ers
          return 'error'
        })

      return fetched
    }
    const queryResult = await fetching()
    //

    // change _state querying
    await isQuerying({
      status: 'afterFetch',
      _settings,
      _fetchState
    })
      .then(res => { return true })
      .catch(ers => { return false })
    //

    // afterFetch
    await lifeCycle({
      status: 'afterFetch',
      type: queryResult,
      _settings,
      _fetchState
    }) // lifeCycle afterFetch

    const isSuccess = queryResult === 'success'

    // afterFetchCheck
    const afterFetchCheck = await lifeCycle({
      status: 'afterFetchCheck',
      type: null,
      _settings,
      _fetchState
    })
      .then(res => { return true })
      .catch(ers => { reject(ers); return false })
    if (!afterFetchCheck) {
      afterAll({
        isSuccess: false,
        error: 'afterFetchCheck',
        _settings,
        _fetchState
      })
      return false
    }
    //

    if (isSuccess) {
      const successFlow = async function () {
        if (afterFetchCheck) {
          await resolve(_response)
        }
      }
      successFlow()
    } else {
      const errorFlow = async function () {
        await reject(_response)
      }
      errorFlow()
    }

    afterAll({
      type: 'all',
      isSuccess,
      error: _response,
      _settings,
      _fetchState
    })
    return isSuccess
  })
} // all
