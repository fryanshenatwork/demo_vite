import { permissionStore } from '/@/plugins/permission/index.ts'

const state = {
  enabled: {
    useCookie: true,
    default: false
  },
  clickedTimes: 0,
  ...permissionStore
}
const cookieTitle = 'COOKIE_KEY'
const encryptKey = 'ENCRYPTE_KEY'

export { state, cookieTitle, encryptKey }
