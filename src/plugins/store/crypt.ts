import CryptoJS from 'crypto-js'
import { encryptKey } from './configs.ts'

const encrypt = function (obj) {
  const ciphertext = CryptoJS.AES.encrypt(JSON.stringify(obj), encryptKey).toString()
  return ciphertext
}

const decrypt = function (str) {
  if ([undefined, null, ''].includes(str)) {
    return ''
  }
  const bytes = CryptoJS.AES.decrypt(str, encryptKey)
  if (typeof (bytes) === 'string') {
    return ''
  }
  try { bytes.toString(CryptoJS.enc.Utf8) } catch (e) { return '' }
  try { JSON.parse(bytes.toString(CryptoJS.enc.Utf8)) } catch (e) { return '' }
  const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8))
  return decryptedData
}

export default {}
export { encrypt, decrypt }
