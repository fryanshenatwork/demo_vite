import { createStore } from 'vuex'
import cookies from 'js-cookie'
import createPersistedState from 'vuex-persistedstate'

import * as configs from './configs.ts'
import * as crypt from './crypt.ts'

const state = (function () {
  const obj = {}
  const historyContent = [null, undefined, ''].includes(cookies.get(configs.cookieTitle))
    ? {}
    : crypt.decrypt(cookies.get(configs.cookieTitle))

  Object.keys(configs.state).forEach((key) => {
    let theValue = configs.state[key]
    if (
      typeof (configs.state[key]) === 'object'
      && configs.state[key].useCookie === true
      && configs.state[key].default !== undefined
    ) {
      if (historyContent[key] !== undefined) {
        theValue = historyContent[key]
      } else {
        theValue = configs.state[key].default
      }
    }
    obj[key] = theValue
  })
  return obj
}())

export default createStore({
  state () {
    return state
  },
  mutations: {
    _update (state, payload) {
      state[payload.key] = payload.value
    }
  },
  actions: {
    update ({ commit }, payload) {
      commit('_update', payload)
    }
  },
  modules: {},
  plugins: [
    createPersistedState({
      getState (key, state) {
        return crypt.decrypt(cookies.get(configs.cookieTitle))
      },
      setState (key, state) {
        const storeObj = {}
        const usecookieTitles = Object.keys(configs.state).filter((key) => {
          if (
            typeof (configs.state[key]) === 'object'
            && configs.state[key].useCookie === true
          ) {
            return true
          }
          return false
        })
        usecookieTitles.forEach((key) => {
          storeObj[key] = state[key]
        })
        const cryptedContent = crypt.encrypt(storeObj)
        if (cryptedContent.length > 4000) {
          console.error('Store encrypted content is bigger than 4000 bytes')
        }
        cookies.set(
          configs.cookieTitle,
          crypt.encrypt(storeObj),
          {
            path: '/',
            secure: false
          }
        )
      }
    })
  ]
})
