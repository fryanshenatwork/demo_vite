const declareToWindow = (settings: string) => {
  const arr = settings.split('.')

  const action = (type: string, item: null): void => {
    let result: any = ''
    let target: any = null
    arr.forEach((key, i) => {
      if (i === 0) { target = window }
      if (target[key] === undefined) { target[key] = {} }
      if (arr.length === i + 1) {
        if (type === null) { target[key] = '' }
        if (type === 'get') { result = target[key] }
        if (type === 'set') { target[key] = item; result = true }
      } else {
        target = target[key]
      }
    })
    return result
  }

  action(null, null) // declare init

  return {
    set value (act: any) {
      action('set', act)
    },
    get value () {
      return action('get', null)
    }
  }
}

export default declareToWindow
