import $ from 'jquery'
import { computed } from 'vue'
import { useStore } from 'vuex'

const expireMinutes = 120

const permissionStore = {
  permission: {
    useCookie: true,
    default: {
      personalData: {
        id: '',
        firstName: '',
        lastName: '',
        display: ''
      },
      token: {},
      verified: false, // firstTime token check
      expire: ''
    }
  }
}

const usePermission = () => {
  const store = useStore()
  const state = computed(() => store.state.permission)

  const newExpire = function () {
    const d = new Date()
    return new Date(d.getTime() + 1000 * 60 * expireMinutes)
  }

  const create = (opts = {}) => new Promise((resolve, reject) => {
    const originObj = permissionStore.permission.default
    store.dispatch('update', {
      key: 'permission',
      value: $.extend(
        true, {},
        originObj,
        opts,
        { verified: true, expire: newExpire() }
      )
    })
    resolve(true)
  })

  const refresh = () => new Promise((resolve, reject) => {
    const originObj = store.state.permission
    store.dispatch('update', {
      key: 'permission',
      value: $.extend(
        true, {},
        originObj,
        { verified: true, expire: newExpire() }
      )
    })
    resolve(true)
  })

  const destroy = () => new Promise((resolve, reject) => {
    const originObj = permissionStore.permission.default
    store.dispatch('update', {
      key: 'permission',
      value: $.extend(true, {}, originObj)
    })
    resolve(true)
  })

  return {
    state, create, refresh, destroy
  }
}

export default {}
export { permissionStore, usePermission }
