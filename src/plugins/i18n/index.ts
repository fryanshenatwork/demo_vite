import { createI18n } from 'vue-i18n/'
import enUS from './locales/en-us.json'
import zhTW from './locales/zh-tw.json'

const messages = {
  'en-us': enUS,
  'zh-tw': zhTW
}

const i18n = createI18n({
  locale: 'zh-tw',
  messages
})

export default i18n
export {}
