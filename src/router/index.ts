import { createWebHashHistory, createRouter } from 'vue-router'

import blank from '/@/views/blank.vue'
import home from '/@/components/HelloWorld.vue'
import login from '/@/views/login.vue'
import logout from '/@/views/logout.vue'
import notFound from '/@/views/404.vue'

const routes = [
  {
    meta: { documentTitleKey: 'dashboard', validPermission: true },
    name: 'main',
    path: '/',
    component: blank
  },
  {
    meta: { documentTitleKey: 'login' },
    name: 'login',
    path: '/login',
    component: login
  },
  {
    meta: { documentTitleKey: 'logout' },
    name: 'logout',
    path: '/logout',
    component: logout
  },
  {
    meta: { documentTitleKey: '404' },
    name: '404',
    path: '/404',
    component: notFound
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: () => ({ name: '404' }),
    component: null
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
