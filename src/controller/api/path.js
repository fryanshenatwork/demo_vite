const path = {
  permission: {
    login: '/login',
    refreshToken: '/refreshToken'
  },
  post: {
    path: '/posts/', type: 'http'
  },
  user: {
    path: '/users/', type: 'http'
  }
}

export default path
