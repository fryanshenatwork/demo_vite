import path from './path'

const settings = (function () {
  const result = {
    mainPath: new URL(window.origin).origin,
    useSSL: new URL(window.origin).protocol === 'https:'
  }
  const originSettings = (window as any).API

  if (
    ![null, undefined, '', 'auto'].includes(originSettings.mainPath)
    && (
      originSettings.mainPath.includes('http://')
      || originSettings.mainPath.includes('https://')
    )
  ) {
    result.mainPath = new URL(originSettings.mainPath).origin
  }
  if (
    ![null, undefined, '', 'auto'].includes(originSettings.useSSL)
    && typeof originSettings.useSSL === 'boolean'
  ) {
    result.useSSL = originSettings.useSSL
    result.mainPath = result.mainPath.replace('http://', 'https://')
  }

  return result
}())

const API = (function () {
  const paths = { ...path }
  const loopRequests = (e) => {
    Object.keys(e).forEach((key) => {
      if (
        e[key]
        && typeof e[key] === 'object'
        && Array.isArray(e[key]) === false
        && [null, undefined, ''].includes(e[key].type)
      ) {
        loopRequests(e[key])
      } else if (
        typeof e[key] === 'string'
        && !e[key].includes('http://')
        && !e[key].includes('https://')
        && !e[key].includes('wss://')
        && !e[key].includes('ws://')
      ) {
        e[key] = settings.mainPath + e[key].split(':id')[0]
      } else if (
        typeof (e[key] === 'object')
        && Array.isArray(e[key]) === false
        && e[key].type === 'ws'
        && e[key].path !== undefined
        && !e[key].path.includes('wss://')
        && !e[key].path.includes('ws://')
      ) {
        const protocol = settings.useSSL ? 'wss' : 'ws'
        e[key] = `${protocol}://${settings.mainPath}${e[key].path.split(':id')[0]}`
      } else if (
        typeof (e[key] === 'object')
        && Array.isArray(e[key]) === false
        && e[key].type === 'http'
        && e[key].path !== undefined
        && !e[key].path.includes('http://')
        && !e[key].path.includes('https://')
      ) {
        e[key] = settings.mainPath + e[key].path.split(':id')[0]
      }
    })
  }
  loopRequests(paths)
  return { ...paths, settings }
}())

export default API
export {}
