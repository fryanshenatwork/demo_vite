import api from './api'
import fryanAxios from '/@/plugins/fryan-axios/index.js'

const API = api.post

const postController = {
  getPost (opts: {
    id: '',
    data: {}
  }) {
    let url = API
    if (opts.id && typeof (opts.id) === 'number') {
      url += `${opts.id}/`
    }

    return new Promise((resolve, reject) => {
      fryanAxios.single({
        method: 'get',
        url,
        ...opts
      })
        .then((res) => resolve(res))
        .catch((ers) => reject([]))
    })
  }
}
export default postController
