const fs = require('fs')
const packageData = require('../package.json')

const data = {
  version: `${packageData.name}-v${packageData.version}`
}

fs.writeFile('./src/.info.json', JSON.stringify(data), (err) => {
  if (err) {
    throw err
  }
})
