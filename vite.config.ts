import path from 'path'
import getPort from 'get-port' // eslint-disable-line
import yargs from 'yargs' // eslint-disable-line

const srcPath = path.resolve(__dirname, './src')

const proxy = (async function () {
  const theNull = [null,
    '',
    undefined]
  const [, , ...processParams] = process.argv
  const args = yargs(processParams).argv
  let thePort = theNull.includes(args.port) ? 8080 : args.port
  const theHost = theNull.includes(args.host) ? '0.0.0.0' : args.host

  thePort = await getPort({
    host: theHost,
    port: getPort.makeRange(thePort, thePort + 10)
  })
    .then((res) => res)

  return {
    host: theHost,
    port: thePort
  }
}())

module.exports = {
  publicPath: './',
  resolvers: [{
    function (filePath) {
      if (filePath.startsWith(srcPath)) {
        return `/@/${path.relative(srcPath, filePath)}`
      }
      return null
    },
    requestToFile (publicPath) {
      if (publicPath.startsWith('/@/')) {
        return path.join(srcPath, publicPath.replace(/^\/@\//, ''))
      }
      return null
    }
  }],
  ...proxy
}
